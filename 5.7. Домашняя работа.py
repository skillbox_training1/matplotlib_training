import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('vgsales.csv')
print(df.head())

# Задача № 1
series = df['JP_Sales']
plt.hist(series, bins = 20)
bin_width = series.max()/20
x_range = [i * bin_width for i in range(21)]
plt.xticks(x_range, rotation = 90)
plt.title('Гистограмма распределения объема продаж всех игр в Японии.')
plt.xlabel('млн. копий')
plt.ylabel('# Записей')
print(plt.show())
print(series.max())

# Задачач № 2
"""data_grand = df[df['Name'] == 'Grand Theft Auto V'].groupby('Platform')['Global_Sales'].mean()
print(data_grand)
data_grand_1 = data_grand.sort_values(ascending = False).reset_index()
plt.bar(data_grand_1['Platform'], data_grand_1['Global_Sales'])
plt.title("Сравнение общемирового объема продаж игры \n 'Grand Theft Auto V' на разных платформах")
plt.xlabel('Платформы')
plt.ylabel('млн. копий')
print(plt.show())"""

# Задача № 3
"""df_1 = df.groupby('Year')['Global_Sales'].sum()
plt.plot(df_1)
plt.xlabel('Год')
plt.ylabel('Продажи, млню копий')
plt.title('Динамика суммарных общемировых продаж всех игр по годам')
print(plt.show())"""

# Задача № 4
"""df_2 = df.groupby('Year')[['NA_Sales', 'Global_Sales']].sum().reset_index()
df_2 = df_2.sort_values(by = ['Year'], ascending = True).dropna()
print(df_2)
plt.scatter(df_2['Year'], df_2['NA_Sales'], label = 'NA_SAles')
plt.scatter(df_2['Year'], df_2['Global_Sales'], label = 'Global_Sales')
plt.legend(loc = 'upper left')
plt.title('Существование зависимости продаж во всем мире \n от продаж в Северной Америке')
plt.xlabel('Год')
plt.ylabel('млн. копий')
print(plt.show())"""

# Задание № 5
"""df_3 = df[df['Name'] == 'Super Mario Bros.'].groupby('Platform')['Global_Sales'].sum()
print(df_3)
plt.pie(df_3, labels = df_3.index, autopct = '%1.1f%%')
plt.title("Процентное состношение продаж игры 'Super Mario Bros.' \n на разных платформах во всем мире за все время.")
print(plt.show())"""

# Задание № 6
"""df_4 = df[df['Year'] == 2013].groupby('Publisher')['Global_Sales'].sum('1').reset_index()
print(df_4)
df_4 = df_4.sort_values(by = ['Global_Sales'], ascending = False)
df_4 = df_4.head(5)
plt.bar(df_4['Publisher'], df_4['Global_Sales'])
plt.title('5 Издателей, имеющих наибольшее суммарные продажи во \n во всем мире в 2013 году')
plt.xlabel('Издатели')
plt.ylabel('млн. копий')
plt.xticks(rotation = 25)
print(plt.show())"""

# Задание № 7
"""df_5 = df[(df['Year'] >= 2000) & (df['Year'] <= 2015) & (df['Publisher'] != 'Nintendo')]['Global_Sales']
print(df_5)

plt.hist(df_5, bins = 20)
plt.title('Распределение величины общемировых продаж игр, \n выпущенных не издателем Nintendo, \n'
          'в период с 2000 по 2015 год включительно')
plt.xlabel('млн. копий')
plt.ylabel('# записей')
bin_width = df_5.max()/20
x_range = [i * bin_width for i in range(21)]
plt.xticks(x_range, rotation = 45)
print(plt.show())"""

# Задание № 8
"""df_6 = df[df['Genre'] == 'Action'].groupby('Year')['Global_Sales'].sum()
plt.plot(df_6)
plt.xlabel('Год')
plt.ylabel('млн. копий')
plt.title('Динамика суммарных мировых продаж игр жанра Action по годам')
# x_range = list(range(df_6.index.min(), df_6.index.max()))
# plt.xticks(x_range)
print(plt.show())
print(df_6)"""

# Задание № 9
"""df_7 = df[df['Publisher'] == 'Microsoft Game Studios'].groupby('Genre')['NA_Sales'].sum().reset_index()
df_7 = df_7.sort_values(by=['NA_Sales'], ascending = False)
plt.bar(df_7['Genre'], df_7['NA_Sales'])
plt.title('Объем продаж игр издателя Microsoft Game Studios \n в Северной Америке за все время в зависимости от жанра')
plt.xlabel('Жанр')
plt.ylabel('Млн. копий')
plt.xticks(rotation = 50)
print(plt.show())
print(df_7)"""

# Задание 10
'''print(df)
df['Ошибка'] = abs(df['NA_Sales'] + df['EU_Sales'] + df['JP_Sales'] + df['Other_Sales'] - df['Global_Sales']) >= 0.01
df_8 = df.groupby('Ошибка')['Ошибка'].count()
plt.pie(df_8, labels = ['Корректное \n суммирование', 'Ошибка \nсуммирования'], autopct = '%1.2f%%')
plt.title('Процент записей в датасете, \n имеющих ошибку суммирования продаж')
print(plt.show())
print(df_8)'''