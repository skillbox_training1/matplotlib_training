
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('vgsales.csv')
print(df.head())

# Задание № 1
'''fig, ax = plt.subplots(figsize=(12, 6))
data = df[df['Genre'] == 'Sports'].groupby('Year')['JP_Sales'].sum().reset_index()
ax.plot(data['Year'], data['JP_Sales'])

ax.set_title('Объем продаж всех игр жанра \"Sports\" в Японии по годам')
ax.set_xlabel('Год')
ax.set_ylabel('Продажи, млн. копий')
print(plt.show())'''


# Задание № 2
'''data = df[df['Publisher'] == 'Activision'].groupby('Year')[['NA_Sales', 'EU_Sales', 'JP_Sales', 'Global_Sales']]\
    .sum().reset_index()

fig, ax = plt.subplots(figsize=(12, 6))
for Country in ['NA_Sales', 'EU_Sales', 'JP_Sales', 'Global_Sales']:
    ax.plot(data['Year'], data[Country], label=Country)

fig.suptitle("Динамика продаж студии \"Activision\" В Северной Америке, Европе, Японии и во всем мире.", y=0.96)
ax.set_xlabel('Год')
ax.set_ylabel('Количество продаж, млн. копий')
ax.legend(loc='upper left')
ax.set_xticks(list(range(int(data['Year'].min()), int(data['Year'].max()), 2)) + [int(data['Year'].max())])
ax.axvline(x=2007, color='gray', linewidth=1, linestyle='--')
print(plt.show())'''

# Задание № 3
'''fig, axs = plt.subplots(nrows = 4, figsize=(12,8))

data = df[df['Publisher'] == 'Activision'].groupby('Year')[['NA_Sales', 'EU_Sales', 'JP_Sales', 'Other_Sales']]\
    .sum().reset_index()

Country = ['NA_Sales', 'EU_Sales', 'JP_Sales', 'Other_Sales']

for i, country in enumerate(Country):
    data_1 = data[country]
    data_0 = data['Year']
    ax =axs[i]
    ax.plot(data_0, data_1, label = country)
    ax.set_xticks(list(range(int(data["Year"].min()), int(data['Year'].max()), 2)) + [int(data['Year'].max())])
    if i == len(Country)-1:
        ax.set_xlabel('Год')
    ax.set_ylabel('Продажи, \n млн. копий')
    ax.legend(loc='upper left')
    print(data_1)
fig.suptitle('Динамика продаж студии \"Activision\"', y = 0.95, fontsize = 18)
print(data)
print(plt.show())'''

# Задание № 4
'''fig, axs = plt.subplots(nrows = 4, figsize=(12,8))

data = df[df['Publisher'] == 'Activision'].groupby('Year')[['NA_Sales', 'EU_Sales', 'JP_Sales', 'Other_Sales']]\
    .sum().reset_index()

Country = ['NA_Sales', 'EU_Sales', 'JP_Sales', 'Other_Sales']

for i, country in enumerate(Country):
    data_1 = data[country]
    data_0 = data['Year']
    ax =axs[i]
    ax.plot(data_0, data_1, label = country)
    ax.set_xticks(list(range(int(data["Year"].min()), int(data['Year'].max()), 2)) + [int(data['Year'].max())])
    if i == len(Country)-1:
        ax.set_xlabel('Год')
    ax.set_ylabel('Продажи, \n млн. копий')
    ax.legend(loc='upper left')
    print(data_1)
fig.suptitle('Динамика продаж студии \"Activision\"', y = 0.95, fontsize = 18)
print(data)
print(plt.show())'''

# Задание № 5
"""data = df[(df['Publisher'] == "Microsoft Game Studios") & (df['Year'] >= 2010)].groupby('Year')['Other_Sales'].sum()
data_1 = df[(df['Publisher'] == "Take-Two Interactive") & (df['Year'] >= 2010)].groupby('Year')['Other_Sales'].sum()

fig, ax = plt.subplots(figsize = (12, 6))
_, bins, _ = ax.hist(data_1, label = 'Take_Two Interactive', alpha = 0.5, bins =8)
ax.hist(data, label = 'Microsoft Game Studios' , alpha = 0.5, bins = bins)

ax.set_title('Распределение мировых продаж игр издателей \n \"Microsoft Game Studios\" и '
             '\"Take-Two Interactive\" после 2010 года')
ax.set_xlabel('Объем продаж, млн. копий')
ax.set_ylabel('# записей')
ax.set_xticks(list(bins))
ax.legend()

print(plt.show())"""

# Задание № 6
"""data = df.groupby('Platform')['JP_Sales'].sum().reset_index()
data = data.sort_values(by = ['JP_Sales'], ascending=False).reset_index()

data['Color'] = 'gray'
data.loc[data['JP_Sales'] >= 175.57, 'Color'] = 'green'
data.loc[data['JP_Sales'] <= 98.65, 'Color'] = 'red'

fig, ax = plt.subplots(figsize = (12,6))
ax.bar(data.head(5)['Platform'], data.head(5)['JP_Sales'], color = data['Color'], alpha = 0.8)

ax.set_title('Топ 5 платформ , для которых в Японии было проданно \n больше всего игр за все время', y = 1.03)
ax.set_xlabel('Название платформ')
ax.set_ylabel('Продажи, млн. копий')
print(plt.show())"""

# Задание № 7

print(df.head())
data = df[df['Publisher'] == 'Nintendo'].groupby('Year')['Name'].count().reset_index()
data_1 = data[data['Name'] >= 35]
print(data_1)
print(data.head(50))
fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data['Year'], data['Name'], linewidth = 2)

ax.axvspan(xmin=data_1["Year"].min(), xmax=data_1['Year'].max(), color = 'green', alpha = 0.3)
ax.axhline(35, color ='green', linewidth =1.5, linestyle = '--', alpha = 0.5)
ax.grid(linestyle ='--')

ax.set_title('Количество игр, выпускаемых Nintendo, по годам')
ax.set_xlabel('Год')
ax.set_ylabel('Количество выпуска игр')
ax.set_xticks(list(range(int(data['Year'].min()), int(data['Year'].max()), 2)))
ax.set_yticks(list(range(data['Name'].min(), data['Name'].max(), 5)))
print(plt.show())

