import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

df = pd.read_csv('wage-data-coast_1.csv')
'''print(df.head())
fig, ax = plt.subplots(figsize = (8, 6))
data = df[(df['State'] == 'Arizona') & (df['Year'] <= 2000)][['Year', 'Salary']]
ax.plot(data['Year'], data['Salary'])
ax.set_title('Минимальная зарплата в штате Аризон до 2000 г.', color = 'navy')
ax.set_xlabel('Год', color = 'navy')
ax.set_ylabel('Минимальная зарплата, $/час', color = 'navy')
ax.set_xticks(list(range(data['Year'].min(), data['Year'].max(), 3)) + [data['Year'].max()])
print(plt.show())
print(data)'''

'''data = df[df['State'] == 'Washington'][['Year', 'Salary']]
data1 = df[df['State'] == 'California'][['Year', 'Salary']]
fig, ax = plt.subplots(figsize=(12, 6))
ax.plot(data['Year'], data['Salary'], label = 'Washington')
ax.plot(data1['Year'], data1['Salary'], label = 'California')
ax.axvline(x=1999, color = 'green', linewidth = 1, linestyle = '--')
ax.set_title('Минимальная зарплатав штате Вашингтон по годам')
ax.set_xlabel('Год')
ax.set_ylabel('Минимальная зарплата, $/час')
ax.legend()
ax.set_xticks(list(range(data['Year'].min(), data['Year'].max(), 5)) + [data['Year'].max()])
print(plt.show())'''

'''data = df[df['State'] == 'Washington'][['Year', 'Salary']]
fig, ax = plt.subplots(figsize=(8, 4))
ax.plot(data['Year'], data['Salary'], label = 'Washington')
ax.axvline(x = 1999)
ax.set_title('Минимальная заработная плата в штате \n Вашингтон по годам')
ax.set_xlabel('Год')
ax.set_ylabel('Минимальная зарплата, $/час')
ax.set_xticks(list(range(data['Year'].min(), data['Year'].max(), 5)) + [data['Year'].max()])
print(plt.show())'''

data = df[df['Year'] == 2000]['Salary']
data1 = df[df['Year'] == 2010]['Salary']

fig, ax = plt.subplots(figsize=(12, 6))
_, bins1,_ = ax.hist(data1, label='2010', alpha=0.5, bins=20)
ax.hist(data, label='2010', alpha=0.5, bins=bins1)
ax.set_title('Распределение минимальной зарплаты в 2000 и 2010 годах')
ax.set_xlabel('Минимальная зараплата, $/час')
ax.set_ylabel('# записей')
ax.set_xticks(list(bins1))
ax.legend()
print(plt.show())

'''for year in [2000, 2010]:
    data = df[df['Year'] == year]['Salary']
    fig, ax = plt.subplots(figsize = (12, 6))
    _, bins1,_ = ax.hist(data, label = str(year), bins = 20)

    ax.set_title('Распределение минимальных зарплат в' + str(year) + 'год')
    ax.set_xlabel('Минимальная зарплата, $/час')
    ax.set_ylabel('# записей')
    ax.set_xticks(list(bins1))

    print(plt.show())'''

'''data = df[df['Year'] == 1980]['Salary']
data1 = df[df['Year'] == 1990]['Salary']
data2 = df[df['Year'] == 2000]['Salary']

fig, ax = plt.subplots(figsize = (12, 6))
_, bins1,_ = ax.hist(data2, label = '2000', alpha = 0.5, bins = 20)
ax.hist(data1, label = '1990', alpha = 0.5, bins = bins1)
ax.hist(data, label = '1980', alpha = 0.5, bins = bins1)

ax.set_title('Распределение минимальной заработной платы в 1980, 1990 и 2000 годах ')
ax.set_xlabel('Минимальная зарплата, $/час')
ax.set_ylabel('# Записей')
ax.set_xticks(list(bins1))
ax.legend()

print(plt.show())'''

'''fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(12, 6))

data = df[df['Year'] == 2000]['Salary']
data1 = df[df['Year'] == 2010]['Salary']
axs[0].set_title('Распределение минмиальнх зарплат в 2000 году')
axs[0].set_ylabel('# записей')

axs[1].set_title('Распределение минмиальнх зарплат в 2010 году')
axs[1].set_xlabel('Минимальная зарплата, $/час')
axs[1].set_ylabel('# записей')

_, bins,_ = axs[1].hist(data1, bins = 20)
axs[0].hist(data, bins = bins)

for ax in axs:
     ax.set_xticks(bins)

     for bin_ in bins:
         ax.axvline(x = bin_, color = 'lightgray', linewidth = 1, linestyle = '--')

print(plt.show())'''

fig, axs = plt.subplots(nrows = 4, ncols = 2, figsize=(12,10), sharey = True)

years = [1980, 1990, 2000, 2010]
is_coastal = [0, 1]

for i, year in enumerate(years):
    for j, is_c in enumerate(is_coastal):
        data = df[(df['Year'] == year) & (df['IsCoastal'] == is_c)]["Salary"]
        ax = axs[i][j]
        ax.hist(data)

        if i == len(years)-1:
            if j == 0:
                state_type = 'Внутренние'
            else:
                state_type = 'Прибрежные'
            ax.set_xlabel('Минимальная зарплата, $/час;' + state_type + ' штаты')

        if j == 0:
            ax.set_ylabel('# записей; ' + str(year) + ' год')

fig.suptitle('Распределение минимальной зарплаты в прибрежных и внутренних штатах \n в 1980-2010 годах', y = 0.97)
print(plt.show())



