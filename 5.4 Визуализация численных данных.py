import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('wage-data.csv')
print(df.head())

series = df[df['Year'] == 2010]['Salary']
print(series.max())
print(series.min())
print(series)

"""plt.hist(series, bins =10)
plt.title('Гистограмма минимальной зарплаты в 2010 году')
plt.xlabel('$/час')
plt.ylabel('# записей')
print(plt.show())

bin_width = series.max()/10
x_range = [i * bin_width for i in range(11)]
plt.hist(series, bins =10)
plt.title('Гистограмма минимальной зарплаты в 2010 году')
plt.xlabel('$/час')
plt.ylabel('# записей')
plt.xticks(x_range)
print(plt.show())

plt.scatter(df['Year'], df['Salary'])
plt.title('Минимальная заработная плата по годам')
plt.xlabel('Год')
plt.ylabel('$/час')
print(plt.show())

df = pd.read_csv('wage-data-coast.csv')
print(df)
plt.scatter(df['Year'], df['Salary'], c = df['IsCoastal'])
plt.title("Минимальная зарплата по годам")
plt.xlabel('Год')
plt.ylabel('$/час')
print(plt.show())"""

"""df = pd.read_csv('wage-data-coast.csv')
df0 = df[df['IsCoastal'] == 0]
plt.scatter(df0['Year'], df0['Salary'], label = 'Not Coastal')

df1 = df[df['IsCoastal'] == 1]
plt.scatter(df1['Year'], df1['Salary'], label = 'Coastal')

plt.title('Минимальная зарплата по годам')
plt.xlabel('Год')
plt.ylabel('$/час')

plt.legend()

print(plt.show())"""

"""df = pd.read_csv('wage-data-coast.csv')
print(df.head())
series = df[(df['Year'] == 2000) & (df['IsCoastal'] == 1)]['Salary']
plt.hist(series, bins = 10)
bin_width = series.max()/10
x_range = [i * bin_width for i in range(11)]
plt.title('Минимальная зарплата в прибрежных штатах в 2000 году')
plt.xlabel('$/час')
plt.ylabel('# штатов')
plt.xticks(x_range)
print(plt.show())"""

df = pd.read_csv('wage-data-coast.csv')
print(df)

df_1 = df[df['Year'] == 2015].groupby('IsCoastal')['Salary'].mean()
print(df_1)

df_1 = df_1.sort_values(ascending = False).reset_index()
print(df_1)

df_1['IsCoastalStr'] = np.where(df_1['IsCoastal'] == 1, 'Coastal', 'Not coastal')
print(df_1)

plt.bar(df_1['IsCoastalStr'], df_1['Salary'])
plt.title("Среднее значение минимальной зарплаты в прибрежных и неприбрежных штатах в 2015 году")
plt.xlabel('Тип штата')
plt.ylabel('$/час')
print(plt.show())

# Круговая диаграмма
