
# Pyplot API

#  Plot - функция отображающая с помощью линий и точек данные по x и y

import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv('wage-data.csv')
print(df.head())

"""series = df[df['State'] == 'California'].set_index(['Year'])['Salary']
print(series)
plt.plot(series)
print(plt.show())"""

series = df.groupby('Year').agg('mean')
print(series)
plt.plot(series)
plt.title('Динамика минимальной зарплаты в Калифорнии по годам!')
plt.xlabel('Год')
plt.ylabel('Минимальная зарплата, $/час')
print(series.index.min())
print(series.index.max())
x_range = list(range(series.index.min(), series.index.max()+1, 10))
print(x_range)
x_range.append(series.index.max())
print(x_range)
plt.xticks(x_range, rotation=45)
print(plt.show())

x_range = list(range(series.index.min(), series.index.max()+1, 10))
x_range.append(series.index.max())
